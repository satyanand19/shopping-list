
const body = document.querySelector('body');
const container = document.createElement('div');
const dropDown = document.createElement('select');
const user1 = document.createElement('option');
const input = document.createElement('input');
const btn = document.createElement('button');
const user2 = document.createElement('option');
const user3 = document.createElement('option');
const user0 = document.createElement('option');

const ul1 = document.createElement('ul');
const ul2 = document.createElement('ul');
const ul3 = document.createElement('ul');

user0.setAttribute('value', '');
user0.textContent = 'select User';
user1.setAttribute('value', 'satya');
user1.textContent = 'Satya';
user2.setAttribute('value', 'shiva');
user2.textContent = 'Shiva';
user3.setAttribute('value', 'sakshi');
user3.textContent = 'Sakshi';
btn.classList.add('button');
btn.textContent = "Add Item";


dropDown.setAttribute('name', 'users');
input.setAttribute('type', 'text');
input.setAttribute('placeholder', 'add item');

container.style.backgroundColor = "grey";

// container.style.padding = "300px";
dropDown.style.margin = "20px";
dropDown.style.padding = "5px";
dropDown.style.fontSize = "1.5rem";
dropDown.style.borderRadius = "8px";
input.style.fontSize = "1.5rem";
btn.style.fontSize = "1.5rem";
btn.style.borderRadius = "1rem";
btn.style.backgroundColor = "black";
btn.style.color = "white";
btn.style.padding = ".5rem";
btn.style.margin = "2rem";
// dropDown.style.border = "0";
dropDown.style.backgroundImage = `linear-gradient(to left, #2E3192 ,#1BFFFF)`;



body.appendChild(container);
container.append(dropDown, input, btn);
dropDown.append(user0, user1, user2, user3);

let usersData = {
    satya: [],
    shiva: [],
    sakshi: []
}


function addValueToList(box, data) {
    const details = document.createElement('div');
    details.style.backgroundColor = "black";
    const heading = document.createElement('h3');
    heading.style.color = 'white';
    heading.style.fontSize = '1.5rem';
    heading.classList.add('item');
    const completed = document.createElement('input');
    completed.type = 'checkbox';
    const btn1 = document.createElement('button');
    btn1.classList.add('removeItem');
    heading.textContent = data;
    btn1.textContent = 'X';
    details.append(heading, completed, btn1);
    box.append(details);

    removeItemFromList(btn1);
    statusOfCompletion(completed);
}

function statusOfCompletion(status) {
    status.addEventListener('click',()=> {
        if(status.checked) {
            status.previousElementSibling.style.color = 'red';
            status.previousElementSibling.textDecoration = `line-through`;
        }
        else {
            status.previousElementSibling.style.color = 'white';
        }
        
    })
}

function displayItems(username) {
    if (body.querySelector('.box')) {
        body.querySelector('.box').remove();
    }
    const box = document.createElement('div');
    box.classList.add('box');
    if (username != '') {
        for (let data of usersData[username]) {
            addValueToList(box, data);
        }
        body.appendChild(box);

    }
}

function removeItemFromList(button) {
    button.addEventListener('click', () => {
        button.parentElement.remove();
        test.removeItem(dropDown.value,button.parentElement.firstElementChild)
        
    });

}

dropDown.addEventListener('change', (e) => {
    displayItems(e.target.value);
});

function addItem() {

    return {
        addItem : (username,item) => {
           usersData[username].push(item);
        },
        removeItem : (username,item) => {
           usersData[username].splice(usersData[username].indexOf(item),1);
        }
    }
}
let test = addItem();

btn.addEventListener('click', () => {
    if(dropDown.value) {
        
    const box = document.querySelector('.box');
    if (box && input.value) {
        test.addItem(dropDown.value, input.value);
        addValueToList(box, input.value);
        input.value = "";
    }
    else {
        alert("Enter the Item");
    }
  } else {
    alert("Select the user");
  }

})





// const user = {
//     Mike : [],
//     John : [],
//     Smith : []
// }

// function addAndRemove(username) {
//     const items = [];
//     const userName = username;
//    return  {
//        addItem : (item) => {
//            items.push(item);

//        },
//        removeItem : (item) => {
//            user[userName].splice(user[userName].indexOf(item),1);
//            console.log(user[userName]);
           
//     }
//    }
// }



// let Mikeoperation = addAndRemove('Mike');
// operation.addItem('Aloo');
// operation.addItem('kalam');
// operation.addItem('pen');
// operation.removeItem('Mike','Aloo');
// console.log(user)
// let Johnoperation1 = addAndRemove('John');
